<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invite extends Model
{
    public $incrementing = false;
    public function is_accepted()
    {
        if($this->is_accepted){
            return "Accepted";
        }
        else{
            return "Not Accepted";
        }
    }
    public function user()
    {
        return $this->belongsTo("App\User","email","user_email");
    }
}

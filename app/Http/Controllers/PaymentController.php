<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Notifications\SubscriptionSuccess;
use Instamojo\Instamojo;
use App\Activity;
class PaymentController extends Controller
{
    private function apiCall(){
        $api = new Instamojo('test_cb4c9be28efa732f536d7390aae', 'test_833387d4eadb027f5fe7aa5c5a5', 'https://test.instamojo.com/api/1.1/');
        return $api;
    }
    public function index(Request $request)
    {
        $user = auth()->user();
        $settings = \App\Setting::all();

        // Collect Data
        $purchase               = collect();
        $purchase->course       = $settings->where('option','course_name')->first()->value;
        $purchase->gst          = $settings->where('option','gst')->first()->value;
        $purchase->fees         = $settings->where('option','fees')->first()->value;
        $purchase->total        = $purchase->fees;
        if($request->code){
            $code = \App\Coupon::where('code',$request->code)->first();
            if($code){
                if($code->expiry > now()){
                    $purchase->codeerror = "Coupon Code Expired";
                }
                elseif($code->user_id !==null && $code->user_id !== $user->id){
                    $purchase->codeerror = "You are not Eligible for this Coupon Code";
                }
                else{
                    $purchase->coupon = $code;
                    if($code->type == "percentage"){
                        $ams = $purchase->fees * $code->discount / 100;
                    }
                    else{
                        $ams =  $code->discount;
                    }

                    $purchase->coupon->discount_amount = $ams;
                    $purchase->total = $purchase->fees - $purchase->coupon->discount_amount;
                }
            }
            else{
                $purchase->codeerror = "Invalid Coupon code";
            }
        }

        $purchase->gst_amount       = $purchase->total * $purchase->gst/100;
        $purchase->total            = $purchase->gst_amount + $purchase->total;


        // Check Invitation
        $invitation = \App\Invite::where('user_email',$user->email)->first();
        if($invitation){
            return view('payment.invitation');
        }
        else{
            return view('payment.confirmation',['purchase'=>$purchase]);
        }
    }
    public function process(Request $request)
    {
        // return $request;
        try {
            $response = $this->apiCall()->paymentRequestCreate(array(
                "purpose" => "Subscription at HiredByCanada",
                "amount" => $request->total,
                "buyer_name" => auth()->user()->name,
                "email" => auth()->user()->email,
                "send_email" => true,
                "send_sms" => false,
                "allow_repeated_payments" => false,
                "redirect_url" => route('payment.confirmation')
                ));
            session(['purchase' => json_encode(['data'=>$request->all(),'response'=>$response])]);
           return redirect($response['longurl']);
        }
        catch (Exception $e) {
            print('Error: ' . $e->getMessage());
        }        
    }
    public function confirmation(Request $request)
    {
        try {
            $response = $this->apiCall()->paymentRequestPaymentStatus($request->payment_request_id, $request->payment_id);
        }
        catch (Exception $e) {
            return  $e->getMessage();
        }
        $user = auth()->user();
        $session = json_decode(session('purchase'));
        if($session->data->total == $response['amount'] &&  $response['status'] == "Completed"){
            // Create Payment
            $payment = new \App\Subscription;
            $payment->user_id = $user->id;
            $payment->coupon_id = (isset($session->data->coupon)) ? $session->data->coupon:null;
            $payment->txn_id = $request->payment_id;
            $payment->txn_method = $response['payment']['instrument_type']." - ".$response['payment']['billing_instrument'];
            $payment->txn_value = $response['amount'];
            $payment->expiry = now()->addYear(); 
            $payment->save();
            $user->notify(new SubscriptionSuccess($payment));
            return redirect()->route('home')->with('success',"Your Subscription is now Active");
        }
        else{
            return view('payment.failed',['response'=>(object)$response]);
        }
    }
    public function invitation()
    {
        $user = auth()->user();
        $invitation = \App\Invite::where('user_email',$user->email)->first();
        if(!$invitation){
            abort(404);
        }
        // Create Payment
        $invitation->is_accepted = true;
        $payment = new \App\Subscription;
        $payment->user_id = $user->id;
        $payment->invitation_id = $invitation->id;
        $payment->txn_id = (string) Str::uuid();
        $payment->txn_method = "Invitation";
        $payment->txn_value = 0;
        $payment->expiry = now()->addYear(); 
        $payment->save();
        $invitation->save();
        $user->notify(new SubscriptionSuccess($payment));
        return redirect()->route('home')->with('success',"Your Subscription is now Active");
    }
}

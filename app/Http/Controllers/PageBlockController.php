<?php

namespace App\Http\Controllers;

use App\PageBlock;
use Illuminate\Http\Request;

class PageBlockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pageBlocks = PageBlock::paginate(($request->count)?$request->count:10);
        return view('admin.pageblocks.index',['pageblocks'=>$pageBlocks]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pageblocks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "title"=>"required|string|max:255|unique:page_blocks",
            "content"=>"required|string",
        ]);
        $page = new PageBlock;
        $page->title = $request->title;
        $page->content = $request->content;
        $page->save();
        return redirect()->back()->with('success',"New Page Block Created");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PageBlock  $pageBlock
     * @return \Illuminate\Http\Response
     */
    public function show(PageBlock $pageBlock)
    {
        return view('admin.pageblocks.edit',['pageblock'=>$pageBlock]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PageBlock  $pageBlock
     * @return \Illuminate\Http\Response
     */
    public function edit(PageBlock $pageBlock)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PageBlock  $pageBlock
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PageBlock $pageBlock)
    {
        $request->validate([
            "content"=>"required|string",
        ]);
        $pageBlock->content = $request->content;
        $pageBlock->save();
        return redirect()->back()->with('success',"Page Block Updated");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PageBlock  $pageBlock
     * @return \Illuminate\Http\Response
     */
    public function destroy(PageBlock $pageBlock)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use FFMpeg;
use App\Video;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function page($page)
    {
        if(View::exists('pages.'.$page)){
            return view('pages.'.$page);
        }
        else{
            abort(404);
        }

    }
    public function access()
    {
        $subscription = auth()->user()->subscriptions;
        if(!$subscription){
            abort(404);
        }
        $videos = Video::orderBy('position','asc')->get();
        $approved = false;
        $isfirst = true;
        foreach ($videos as $key) {
            $key->access = auth()->user()->activities()->where('video_id',$key->id)->first();
            if(!$key->access && $isfirst){
                $activity = new \App\Activity;
                $activity->user_id = auth()->user()->id;
                $activity->video_id = $key->id;
                $activity->save();
                $key->access = $activity;
            }
            $isfirst=false;
        }
        return view('access.videos',['videos'=>$videos]);
    }
    public function accessVideo($video)
    {
        $video = Video::findorfail($video);
        $subscription = auth()->user()->subscriptions;
        if(!$subscription){
            abort(404);
        }
        $access = auth()->user()->activities()->where('video_id',$video->id)->first();
        if(!$access){
            abort(404);
        }
        return view('access.video',['video'=>$video]);
    }
    public function Purchase(Request $request)
    {
        if($request->coupon){
            $coupon = \App\Coupon::where('code',$request->coupon);
            if($coupon){
                if($coupon->expiry >= now()){
                    return redirect()->back()->withErrors(['coupon'=>"Coupon Expired"]);
                }
            }
            else{
                return redirect()->back()->withErrors(['coupon'=>"Invalid Coupon"]);
            }
        }
        return view('purchase');
    }
}

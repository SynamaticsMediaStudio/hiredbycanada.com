<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    public function coupon()
    {
        return $this->belongsTo("App\Coupon");
    }
    public function invitation()
    {
        return $this->belongsTo("App\Invite");
    }
}

@extends('errors::minimal')

@section('title', __('Opps.'))
@section('code')
<h1>404</h1>
@endsection
@section('message')
{!!__('We are Unable to find the requested Resource. <a href="/">Go Back Home</a>')!!}
@endsection

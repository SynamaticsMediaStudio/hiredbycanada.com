@extends('layouts.app')
@section('content')
<div class="jumbotron py-4 bg-primary">
    <div class="container">
        <h3 class="text-white mb-4"><span class="text-thin border-bottom py-2 border-white">Access </span>Videos</h3>
    </div>
</div>
<div class="container">
    <div class="row">
        @foreach ($videos as $video)
            @if ($video->access)
            <div class="col-sm-12 col-md-6 col-lg-3 text-primary">
                <a href="{{route("access-video",$video)}}">
                    <div class="card border-0 shadow-sm mb-2">
                        <div class="img-thumbnail border-0"
                            style="height:150px;background-image:url('{{Storage::url($video->thumbnail)}}')">
                            <span class="bg-dark text-light m-1 float-right p-1">{{$video->duration}}</span>
                        </div>
                        <div class="card-footer border-0">
                            {{$video->title}}
                        </div>
                    </div>
                </a>
            </div>
            @else               
            <div class="col-sm-12 col-md-6 col-lg-3 text-primary disabled">
                    <div class="card border-0 shadow-sm mb-2">
                        <div class="img-thumbnail border-0"
                            style="height:150px;background-image:url('{{Storage::url($video->thumbnail)}}')">
                            <span class="bg-dark text-light m-1 float-right p-1">{{$video->duration}}</span>
                        </div>
                        <div class="card-footer border-0">
                            {{$video->title}}
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    </div>
</div>
@endsection

@extends('layouts.app')
@section('content')
    <div class="container py-4 ">
        <div class="card border-0 shadow-sm">
            <div class="card-body">
                <h1 class="text-primary text-thin my-4">Congratulations</h1>
                <p class="text-black-50">@contentblock(invite-congratulations)</p>
                <form action="{{route('payment.invitation')}}" method="post">
                    @csrf
                    <button type="submit" class="btn btn-outline-primary">Confirm Subscription</button>
                </form>
            </div>
        </div>
    </div>
@endsection
@extends('layouts.app')
@section('content')
    <div class="container py-4 ">
        <div class="py-4"></div>
        <h4 class="text-center text-danger">Your Payment Failed</h4>
        <div class="py-3 text-center text-black-50">
            <p>Your Transaction failed due to {{$response->payment['failure']['reason']}} . Here are the details we gathered from the gateway
                {{$response->payment['failure']['message']}}
            </p>
            <a href="{{route('payment.confirm')}}" class="btn btn-primary">Retry Payment</a>
        </div>
    </div>
@endsection
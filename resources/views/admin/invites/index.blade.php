@extends('layouts.admin')
@section('content')
<div class="py-4"></div>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="float-right">
                <a href="#invite-add-modal" data-target="#invite-add-modal" data-toggle="modal" class="btn btn-dark btn-sm">Add Invites</a>
            </div>
            <h4 class="text-primary">Free Access Invitations</h4>
            <p class="text-justify about-para">
                @empty($invites->count())
                <div class="text-center py-4 text-black-50">
                    <i class="fas fa-history fa-3x mb-3"></i>
                    <h3 class="mb-4">No Invites</h3>
                    <a href="#invite-add-modal" data-target="#invite-add-modal" data-toggle="modal" class="btn btn-dark btn-sm">Add Invites</a>
                </div>
                @else
                <table class="table table-sm">
                    <thead class="">
                        <th>Invite ID</th>
                        <th>Invite Email</th>
                        <th>Status</th>
                    </thead>
                    <tbody>
                        @foreach ($invites as $invite)
                            <tr>
                                <td><a href="{{route('invites.show',$invite)}}">{{$invite->id}}</a></td>
                                <td><a href="{{route('invites.show',$invite)}}">{{$invite->user_email}}</a></td>
                                <td><a href="{{route('invites.show',$invite)}}">{{$invite->is_accepted()}}</a></td>
                                <td><a href="{{route('invites.show',$invite)}}">{{$invite->user}}</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                @endempty                
                {{-- {{$invites->links}} --}}
            </p>
        </div>
    </div>
</div>
<div class="modal border-0" id="invite-add-modal" tabindex="-1" role="dialog" aria-labelledby="invite-add-modalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm modal-dialog-centered border-0" role="document">
    <div class="modal-content border-0">
      <div class="modal-header border-0">
        <h5 class="modal-title" id="invite-add-modalLabel">Add Email Addresses</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{route('invites.store')}}" method="POST">
            @csrf
            <label for="emails">Add Email Addresses Sperated by Commas</label>
            <textarea class="form-control" name="emails" id="emails" value="{{old('emails')}}"></textarea>
            <div class="py-3"></div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Continue</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

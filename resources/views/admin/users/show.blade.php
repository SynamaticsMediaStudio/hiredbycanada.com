@extends('layouts.admin')
@section('content')
<div class="jumbotron bg-primary text-white py-4">
    <div class="container">
        <h4>{{$user->name}}</h4>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-4 border-right">
            <div class="card">
                <div class="card-body">
                    <form action="" method="POST">
                        @csrf
                        <input type="hidden" name="_method" value="PUT">
                        <div class="form-group">
                            <label for="search">Name</label>
                            <input type="text" name="search" id="name" class="form-control" value="{{$user->name}}"/>
                        </div>
                        <div class="form-group">
                            <label for="search">Email</label>
                            <input type="email" name="search" id="email" class="form-control" value="{{$user->email}}"/>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-sm btn-primary">Update</button>
                        </div>
                    </form>
                  </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-8">
            <table class="table">
                <tr>
                    <th>Joined On </th>
                    <td>
                        {{Carbon::parse($user->created_at)->toFormattedDateString()}}<br/>
                        <small>{{Carbon::parse($user->created_at)->diffForHumans()}}</small>
                    </td>
                </tr>
                <tr>
                    <th>Subscribtion Status</th>
                    <td>{{$user->subscriptions ? "Active":"Inactive"}}</td>
                </tr>
                @if ($user->subscriptions)
                <tr>
                    <th>Expiry </th>
                    <td>
                        {{Carbon::parse($user->subscriptions->expiry)->toFormattedDateString()}}<br/>
                        <small>{{Carbon::parse($user->subscriptions->expiry)->diffForHumans()}}</small>
                    </td>
                </tr>
                <tr>
                    <th>Transaction ID </th>
                    <td>
                        {{$user->subscriptions->txn_id}}
                    </td>
                </tr>
                <tr>
                    <th>Transaction Method </th>
                    <td>
                        {{$user->subscriptions->txn_method}}
                    </td>
                </tr>
                <tr>
                    <th>Fees Collected </th>
                    <td>
                        {{$user->subscriptions->txn_value}}
                    </td>
                </tr>
                <tr>
                    <th>Coupon Code </th>
                    <td>
                        {{($user->subscriptions->coupon)? $user->subscriptions->coupon->coupon:""}}
                    </td>
                </tr>
                <tr>
                    <th>Invitation Status </th>
                    <td>
                        @if ($user->subscriptions->invitation)
                            {{($user->subscriptions->invitation->is_accepted)?"Accepted":"Not Accepted"}}
                        @else
                        Not Invited
                        @endif
                    </td>
                </tr>
                @endif
            </table>
        </div>
        <div class="col-sm-12 my-4">
            <strong class="text-muted">Videos Accessed</strong>
        </div>
        @foreach ($user->activities as $item)
        <div class="col-sm-12 col-md-6 col-lg-3 mb-3">
            <div class="p-3 border border-primary">
                <div>
                    @if($item->video)
                    <p>{{$item->video->title}}</p>
                    @else
                    <p>Video Deleted</p>
                    @endif
                    <small>Access Granted at {{$item->created_at}}</small>
                </div>
                @if($item->is_completed)
                    <small class="badge badge-success">Video Completed</small>
                @else
                    <small class="badge badge-info">Video not Completed</small>
                @endif
                @if($item->is_activity_completed)
                    <small class="badge badge-success">Activity Finished</small>
                @else
                    <small class="badge badge-info">Activity not Finished</small>
                @endif
                @if(!$item->is_activity_completed)
                <form action="{{route('activity.complete',$item)}}" method="post">
                    @csrf
                    <button type="submit" class="btn btn-success btn-sm" href="{{route('activity.complete',$item)}}">Complete Activity</button>
                </form>
                @endif                
            </div>   
            <div>
            </div>                     
        </div>
        @endforeach
        @empty($user->activities->count())
        No Video Accessed Yet
        @endempty
    </div>
</div>
@endsection

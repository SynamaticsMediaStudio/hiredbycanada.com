@extends('layouts.admin')
@section('content')
<div class="jumbotron bg-primary text-white py-4">
    <div class="container">
        <h4>Users</h4>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-4 border-right">
            <div class="card">
                <div class="card-body">
                    <form action="" method="get">
                        <div class="form-group">
                            <label for="search">Search</label>
                            <input type="search" autocomplete="off" name="search" id="search" class="form-control" value="{{request()->search}}"/>
                        </div>
                        <div class="form-group">
                            <label for="results_no">No of Results</label>
                            <input type="number" name="results_no" id="results_no" class="form-control" value="{{(request()->results_no?request()->results_no:10)}}"/>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-sm btn-primary">Filter</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-8">
            <div class="row">
                @foreach ($users as $user)
                <div class="col-sm-12 col-md-6">
                    <a href="{{route('users.show',$user)}}">
                    <div class="card border-0 shadow-sm mb-4">
                        <div class="card-body">
                            <h6 class="text-primary mb-0">{{$user->name}}</h6>
                            <p class="text-black-50">{{$user->email}}</p>
                            {{$user->isPaid()}}
                        </div>
                    </div>
                    </a>
                </div>
                @endforeach
                <div class="col-sm-12">
                    {{$users->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

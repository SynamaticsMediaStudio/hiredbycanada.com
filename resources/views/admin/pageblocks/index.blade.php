@extends('layouts.admin')
@section('content')
<div class="jumbotron bg-primary text-white py-4">
    <div class="container">
        <div class="float-right">
            <a href="{{route('page-blocks.create')}}" class="btn btn-outline-light btn-sm">Create new Block</a>
        </div>
        <h4>Page Blocks</h4>
    </div>
</div>
<div class="container">
    <div class="row">
        @foreach ($pageblocks as $pageblock)
        <div class="col-sm-12 col-md-6 col-lg-4">
            <a href="{{route('page-blocks.show',$pageblock)}}">
            <div class="card mb-2 border-0 shadow-sm">
                <div class="card-body">
                    {{$pageblock->title}}
                </div>
            </div>
            </a>
        </div>
        @endforeach
    </div>
        @empty($pageblocks->count())
        No Page Blocks Available
        @endempty
    {{$pageblocks->links()}}
</div>
@endsection

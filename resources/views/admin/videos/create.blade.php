@extends('layouts.admin')
@section('content')
<div class="jumbotron bg-primary text-white py-4">
    <div class="container">
        <div class="float-right">
            <a href="{{route('modules.index')}}" class="btn btn-outline-light btn-sm">Back to All </a>
        </div>
        <h4>Upload New Module</h4>
    </div>
</div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-10">
            <div class="card shadow-sm border-0">
                <div class="card-body">
                    <form action="{{route('modules.store')}}" method="post" enctype="multipart/form-data" class="row">
                        @csrf
                        <div class="form-group col-sm-12 col-md-12">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="video" name="video"  accept="video/*">
                                <label class="custom-file-label" for="video">Choose file</label>
                            </div>                            
                        </div>
                        <div class="col-sm-12"></div>
                        <div class="form-group col-sm-12 col-md-5">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="thumbnail"  name="thumbnail"  accept="image/*">
                                <label class="custom-file-label" for="thumbnail">Select Thumbnail</label>
                            </div>                            
                        </div>
                        <div class="form-group col-sm-12 col-md-1 py-2 text-center">
                            <strong>OR</strong>
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Create Thumbnail At</span>
                            </div>
                            <input type="number" name="create_at" id="create_at" class="form-control" min="1" value="{{old('create_at',10)}}" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Seconds</span>
                            </div>                            
                            </div>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="title">Video Title</label>
                            <input type="text" name="title" id="title" class="form-control" value="{{old('title')}}">
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="description">Description</label>
                            <textarea name="description" id="description" class="editor">{{old('description')}}</textarea>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="activity">Activity</label>
                            <textarea name="activity" id="activity" class="editor">{{old('activity')}}</textarea>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="position">Position</label>
                            <input type="number" step="1" min="1" name="position" id="position" class="form-control" value="{{old('position',1)}}"/>
                        </div>
                        <div class="form-group col-sm-12">
                            <button class="btn btn-primary" type="submit">Save and Continue</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

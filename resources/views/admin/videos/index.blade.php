@extends('layouts.admin')
@section('content')
<div class="jumbotron bg-primary text-white py-4">
    <div class="container">
        <div class="float-right">
            <a href="{{route('modules.create')}}" class="btn btn-outline-light btn-sm">Upload New Video </a>
        </div>
        <h4>Modules</h4>
    </div>
</div>
<div class="container">
    <div class="row">
        @foreach ($videos as $video)
        <div class="col-sm-12 col-md-6 col-lg-4">
            <a href="{{route('modules.show',$video)}}">
            <div class="card mb-2 border-0 shadow-sm">
                <div class="img-thumbnail" style="background-image:url('{{Storage::url($video->thumbnail)}}')"></div>
                <div class="card-body">
                    {{$video->title}}
                </div>
                <div class="card-footer border-0">
                    <form action="{{route('modules.destroy',$video)}}" method="post">
                        <input type="hidden" name="_method" value="DELETE">
                        @csrf
                        <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                    </form>
                </div>
            </div>
            </a>
        </div>
        @endforeach
    </div>
        @empty($videos->count())
        No Videos
        @endempty
    {{$videos->links()}}
</div>
@endsection

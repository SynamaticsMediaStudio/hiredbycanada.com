@component('mail::message')
# Your Subscription Activation
@contentblock(email.subscription.success)
{{-- Your Subscription will end at {{$subscription->expiry}} --}}
@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

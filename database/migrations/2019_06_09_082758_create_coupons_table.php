<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Coupon;
class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code')->unique();
            $table->string('discount');
            $table->string('type')->default('percentage');
            $table->string('user_id')->nullable();
            $table->datetime('expiry')->nullable();
            $table->timestamps();
        });
        $coupon = new Coupon;
        $coupon->code = "CX123456";
        $coupon->discount = 20;
        $coupon->expiry = now();
        $coupon->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
